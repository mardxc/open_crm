var data_item = Array();
var data_producto = Array();

$(document).ready(function () {
    fetch_list_empresas();
    draw_list_item(data_item);
});




$('#menu_inventario').click(function (e) { 
    $('#cotizador').css("display", "none");
    $('#historial_cotizacion').css("display", "none");
    $("#inventario").fadeIn(400);

    fetch_list_categoria();
    fetch_list_unidadMedida();
});

$('#menu_cotizador').click(function (e) { 
    $('#inventario').css("display", "none");
    $('#historial_cotizacion').css("display", "none");
    $("#cotizador").fadeIn(400);
});

$('#menu_historial_cotizacion').click(function (e) { 
    $('#inventario').css("display", "none");
    $('#cotizador').css("display", "none");
    $("#historial_cotizacion").fadeIn(400);
    fetch_list_cotizaciones();
});

// EVENTOS DE EMPRESA
$('#btn_agregar_empresa').click(function (){
    $('#modal_agregar_empresa').modal();
});

$('#btn_guardar_empresa').click(function (e) { 
    get_agregar_empresa();    
});
// EVENTOS DE EMPRESA

// EVENTOS DE ITEM
$('#btn_agregar_item').click(function (e) {     
    fetch_list_producto();
    $('#item_medida_utilizar').val(0)
    $('#item_cantidad_utilizar').val(0)
    $('#item_descuento_utilizar').val(0)
    $('#modal_agregar_item').modal('show');
});

$('#item_medida_utilizar').change(function (e) { 
    calcular_importe();
    
});

$('#item_cantidad_utilizar').change(function (e) { 
    calcular_importe();
    
});

$("#table_cotizador_agregar_item tbody").on("click", "input", function() {
    calcular_importe();
});

$('#item_descuento_utilizar').change(function (e) { 
    calcular_importe();
    
});

$('#btn_guardar_item').click(function (e) { 
    get_agregar_item();    
});
// EVENTOS DE ITEM

// LISTAR 
function draw_list_empresas(lista) {
    $('#list_empresas').append(
        `<option value="0">Selecciona una empresa</option>`
    );
    $.each(lista, function (key, val) {
        $('#list_empresas').append(
            `<option value="${val.id_bas_empresa}">${val.nombre_corto}</option>`
        );
      });
 }

 function draw_list_categoria(lista) {
     $('#inventario_producto_categoria').append(
         `<option value="0">Selecciona una categoria</option>`
     );
     $.each(lista, function (key, val) {
         $('#inventario_producto_categoria').append(
             `<option value="${val.id_categoria}">${val.descripcion}</option>`
         );
       });
  }

  function draw_list_unidadMedida(lista) {
      $('#inventario_producto_unidad').append(
          `<option value="0">Selecciona una unidad de medida</option>`
      );
      $.each(lista, function (key, val) {
          $('#inventario_producto_unidad').append(
              `<option value="${val.id_unidad_medidia}">${val.descripcion}</option>`
          );
        });
   }

 function draw_list_item(lista) {     
    $('#table_body_cotizador_item').html('');
    $('#table_cotizador_item').DataTable().clear();
    $('#table_cotizador_item').DataTable().destroy();

    $.each(lista, function (key, val) {
        $('#table_body_cotizador_item').append(`
            <tr id="${val.id_producto}">
              <td>${val.id_producto}</td>
              <td>${val.descripcion}</td>
              <td>${val.precio}</td>
              <td>${val.descuento}</td>
              <td>${val.unidad_medida}</td>
              <td>${val.importe}</td>
            </tr>
        `);
      });

      
      if(! $.fn.DataTable.isDataTable('#table_cotizador_item')) {
        $('#table_cotizador_item').DataTable({
            destroy: true,
            processing: true,
            scrollY: "400px",
            sScrollX: "100%",
            sScrollXInner: "100%",
            bScrollCollapse: true,
            scrollCollapse: true,
            language: {
                lengthMenu: "Mostrando _MENU_ registros por pagina",
                zeroRecords: "Registros no encontrados",
                info: "Mostrando _PAGE_ paginas de _PAGES_",
                infoEmpty: "Registros no disponibles",
                infoFiltered: "(filtered from _MAX_ total records)"
            }
        });
    }
    $('#table_cotizador_item').DataTable().draw();
 }

 function draw_list_productos(lista) {
    $('#table_body_cotizador_agregar_item').html('');
    $('#table_cotizador_agregar_item').DataTable().clear();
    $('#table_cotizador_agregar_item').DataTable().destroy();
    data_producto = lista;
    let i = 0;
    $.each(lista, function (key, val) {
        $('#table_body_cotizador_agregar_item').append(`
            <tr>
            <th scope="row">
                <input type="radio" class="item_producto_utilizar" value="${i}">
            </th>
              <td>${val.nombre}</td>
              <td>${val.precio_unidad}</td>
              <td>${val.medida}</td>
              <td>${val.unidad_medida}</td>
              <td>${val.estatus}</td>
              <td>${val.existencia}</td>
            </tr>
        `);
        i++;
      });

      
      if(! $.fn.DataTable.isDataTable('#table_cotizador_agregar_item')) {
        $('#table_cotizador_agregar_item').DataTable({
            destroy: true,
            processing: true,
            scrollY: "400px",
            sScrollX: "100%",
            sScrollXInner: "100%",
            bScrollCollapse: true,
            scrollCollapse: true,
            language: {
                lengthMenu: "Mostrando _MENU_ registros por pagina",
                zeroRecords: "Registros no encontrados",
                info: "Mostrando _PAGE_ paginas de _PAGES_",
                infoEmpty: "Registros no disponibles",
                infoFiltered: "(filtered from _MAX_ total records)"
            }
        });
    }
    $('#table_cotizador_agregar_item').DataTable().draw();
 }

 

 function draw_list_cotizaciones(lista) {     
    $('#table_body_historial_cotizacion').html('');
    $('#table_historial_cotizacion').DataTable().clear();
    $('#table_historial_cotizacion').DataTable().destroy();

    $.each(lista, function (key, val) {
        $('#table_body_historial_cotizacion').append(`
            <tr id="${val.id_cotizacion}">
              <td>${val.id_cotizacion}</td>
              <td>${val.empresa_cliente}</td>
              <td>${val.correo_cliente}</td>
              <td>${val.nombre_cliente}</td>
              <td>${val.importe_siniva}</td>
              <td>
                <span class="btn btn-primary" onclick="print_cotizacion(${val.id_cotizacion})">
                    <i class="fa fa-file" aria-hidden="true"></i>
                </span>
              </td>
            </tr>
        `);
      });

      
      if(! $.fn.DataTable.isDataTable('#table_historial_cotizacion')) {
        $('#table_historial_cotizacion').DataTable({
            destroy: true,
            processing: true,
            scrollY: "400px",
            sScrollX: "100%",
            sScrollXInner: "100%",
            bScrollCollapse: true,
            scrollCollapse: true,
            language: {
                lengthMenu: "Mostrando _MENU_ registros por pagina",
                zeroRecords: "Registros no encontrados",
                info: "Mostrando _PAGE_ paginas de _PAGES_",
                infoEmpty: "Registros no disponibles",
                infoFiltered: "(filtered from _MAX_ total records)"
            }
        });
    }
    $('#table_historial_cotizacion').DataTable().draw();
 }
// OBTENER DATOS DE MODALES
function get_agregar_empresa(){
    let nombre_corto    = $('#empresa_nombre_corto').val();
    let nombre          = $('#empresa_nombre').val();
    let mail            = $('#empresa_mail').val();
    let tel_fijo        = $('#empresa_tel_fijo').val();
    let tel_movil       = $('#empresa_tel_movil').val();
    let tipo_persona    = $('#list_empresa_persona').val();
    let data = {
        'nombre_corto'  : nombre_corto,
        'empresa'       : nombre,
        'mail'          : mail,
        'tel_fijo'      : tel_fijo,
        'tel_movil'     : tel_movil,
        'tipo_persona'  : tipo_persona,
        'estatus'       : 'Activo'
    }
    agregar_empresa(data);
}

function get_agregar_item(){
    let descuento_utilizar      = $('#item_descuento_utilizar').val();
    let importe                 = $('#item_importe_siniva').val();
    let unidad_medida;
    let precio_unidad;
    let id_producto;
    let descripcion;

    let medida      = $('#item_medida_utilizar').val();
    let cantidad    = $('#item_cantidad_utilizar').val();

    let validate = false;
    $("input:radio").each(function() {
        let val = $('input:radio[class=' + this.className + ']:checked').val();
        if (val != undefined) {
            id_producto     = data_producto[val]['id_producto'];
            descripcion     = data_producto[val]['descripcion'];
            precio_unidad   = data_producto[val]['precio_unidad'];
            unidad_medida   = data_producto[val]['unidad_medida'];
            validate = true;
        }
    });

    if(validate){
        let data = {
            'id_producto'   : id_producto,
            'descripcion'   : descripcion,
            'precio'        : precio_unidad,
            'descuento'     : descuento_utilizar,
            'unidad_medida' : unidad_medida,
            'importe'       : importe,
            'medida'        : medida,
            'cantidad'      : cantidad
        }
        data_item.push(data);
        draw_list_item(data_item);
        $('#modal_agregar_item').modal('hide');
        toast_success('Item agregado');
    }else{
        swal_warning("Selecciona un producto");
    }
}

function calcular_importe(){
    let precio_unidad;

    $("input:radio").each(function() {
        let val = $('input:radio[class=' + this.className + ']:checked').val();
        if (val != undefined) {
            precio_unidad   = data_producto[val]['precio_unidad'];
        }
    });

    let medida      = $('#item_medida_utilizar').val();
    let cantidad    = $('#item_cantidad_utilizar').val();
    let descuento   = $('#item_descuento_utilizar').val();

    let importe_sdescuento = (precio_unidad * medida * cantidad).toFixed(2);
    let importe_descuento = (importe_sdescuento * descuento).toFixed(2) / 100;
    let importe_cdescuento = importe_sdescuento - importe_descuento;
    $('#item_importe_siniva').val(importe_cdescuento);
}

// GAURDAR CATEGORIA
$('#btn_agregar_categoria').click(function (e) {  
    e.preventDefault();
       
    let descripcion    = $('#inventario_categoria').val();
    let data = {
        'descripcion'  : descripcion,
        'estatus'       : 'Activo'
    }
    guardar_categoria(data);
});
// GAURDAR CATEGORIA

// GAURDAR UNIDAD DE MEDIDA
$('#btn_agregar_unidadMedida').click(function (e) {  
    e.preventDefault();
       
    let descripcion    = $('#inventario_unidad').val();
    let data = {
        'descripcion'  : descripcion,
        'estatus'       : 'Activo'
    }
    guardar_unidadMedida(data);
});
// GAURDAR UNIDAD DE MEDIDA

$('#btn_agregar_producto').click(function (e) {  
    e.preventDefault();

    let producto_nombre          = $('#inventario_producto_nombre').val();
    let producto_descripcion     = $('#inventario_producto_descripcion').val();
    let producto_medida          = $('#inventario_producto_medida').val();
    let producto_precio          = $('#inventario_producto_precio').val();
    let producto_existencia      = $('#inventario_producto_existencia').val();
    let producto_categoria       = $('#inventario_producto_categoria').val();
    let producto_unidad          = $('#inventario_producto_unidad').val();

    let data = {
        'nombre'            : producto_nombre,
        'descripcion'       : producto_descripcion,
        'medida'            : producto_medida,
        'precio_unidad'     : producto_precio,
        'estatus'           : 'Activo',
        'existencia'        : producto_existencia,
        'id_categoria'      : producto_categoria,
        'id_unidad_medidia' : producto_unidad
    }
    guardar_producto(data);
});
 // GUARDAR COTIZACION 
    $('#btn_guardar_cotizacion').click(function (e) {  
        e.preventDefault();
        
        let id_empresa  = $('#list_empresas').val();
        let mail        = $('#cotizacion_email').val();
        let atencion    = $('#cotizacion_atencion').val();

        let data = {
            'id_empresa' : id_empresa,
            'mail'       : mail,
            'atencion'   : atencion,
            'item'       : data_item
        }
        guardar_cotizacion(data);
    });

    function print_cotizacion(id) {
        //window.location.replace(url+'pdf_cotizacion/'+id);
        //window.location.href = url+'pdf_cotizacion/'+id;
        window.open(url+'pdf_cotizacion/'+id, '_blank');
    }
 // GUARDAR COTIZACION


 //Configuracion del lenguaje del data table
var config_dataTable = {
    "decimal": "",
    "emptyTable": "No hay información",
    "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
    "infoEmpty": "Mostrando 0 de 0 Entradas",
    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
    "infoPostFix": "",
    "thousands": ",",
    "lengthMenu": "Mostrar _MENU_ Entradas",
    "loadingRecords": "Cargando...",
    "processing": "Procesando...",
    "search": "Buscar:",
    "zeroRecords": "Sin resultados encontrados",
    "paginate": {
      "first": "Primero",
      "last": "Ultimo",
      "next": "Siguiente",
      "previous": "Anterior"
    }
  };

  