function toast_success(msg){
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      
      Toast.fire({
        icon: 'success',
        title: msg
      })
}

function swal_success(msg){
    Swal.fire({
        title: msg,
        icon: 'success',
        timer: 2000,
        showConfirmButton: false,
        timerProgressBar: true,
    });
}

function swal_error(msg){
    Swal.fire({
        title: msg,
        icon: 'error',
        timer: 2000,
        showConfirmButton: false,
        timerProgressBar: true,
    });
}

function swal_warning(msg){
    Swal.fire({
        title: msg,
        icon: 'warning',
        timer: 2000,
        showConfirmButton: false,
        timerProgressBar: true,
    });
}

function toast_info(msg){
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      
      Toast.fire({
        icon: 'info',
        title: msg
      })
}

function swal_info(msg){
    Swal.fire({
        title: msg,
        icon: 'info',
        timer: 2000,
        showConfirmButton: false,
        timerProgressBar: true,
    });
}