//const url = 'http://localhost/bpfsmx-api/';
const url = 'http://estrategiaytecnologia.com/bpfsmx-api/';
function fetch_list_empresas(){
    $.ajax({
        type: "GET",
        url: url+'empresa',
        dataType: "json",
        beforeSend : function(){
            $('#list_empresas').html('');
        },
        success: function (response) {
            if(response.status=='ok'){
                draw_list_empresas(response.body);
            }
        }
    });
}

function fetch_list_item(){
    $.ajax({
        type: "GET",
        url: url+'item',
        //data: "data",
        dataType: "json",
        beforeSend : function(){
            $('#table_body_cotizador_item').html('');
        },
        success: function (response) {
            if(response.status=='ok'){
                draw_list_item(response.body);
            }            
        }
    });
}

function fetch_list_producto(){
    $.ajax({
        type: "GET",
        url: url+'producto',
        //data: "data",
        dataType: "json",
        beforeSend : function(){
            $('#table_body_cotizador_agregar_item').html('');
        },
        success: function (response) {
            if(response.status=='ok'){
                draw_list_productos(response.body);
            }            
        }
    });
}

function fetch_list_categoria(){
    $.ajax({
        type: "GET",
        url: url+'categoria',
        //data: "data",
        dataType: "json",
        beforeSend : function(){
            $('#inventario_producto_categoria').html('');
        },
        success: function (response) {
            if(response.status=='ok'){
                draw_list_categoria(response.body);
            }            
        }
    });
}

function fetch_list_unidadMedida(){
    $.ajax({
        type: "GET",
        url: url+'unidadMedida',
        //data: "data",
        dataType: "json",
        beforeSend : function(){
            $('#inventario_producto_unidad').html('');
        },
        success: function (response) {
            if(response.status=='ok'){
                draw_list_unidadMedida(response.body);
            }            
        }
    });
}

function fetch_list_cotizaciones() {
    $.ajax({
        type: "GET",
        url: url+'cotizacion',
        //data: "data",
        dataType: "json",
        beforeSend : function(){
            $('#table_body_historial_cotizacion').html('');
        },
        success: function (response) {
            if(response.status=='ok'){
                draw_list_cotizaciones(response.body);
            }            
        }
    });    
}

//// AGREGAR
function agregar_empresa(data){
    $.ajax({
        type: "POST",
        url: url+'empresa',
        data: {data},
        dataType: "json",
        beforeSend : function(){
        },
        success: function (response) {
            if(response.status=='ok'){
                fetch_list_empresas();
                toast_success(response.body);
            }
        }
    });
}

function guardar_cotizacion(data){
    $.ajax({
        type: "POST",
        url: url+'cotizacion',
        data: {data},
        dataType: "json",
        beforeSend : function(){
        },
        success: function (response) {
            if(response.status=='ok'){
                swal_success(response.body);
                $('#list_empresas').val(0);
                $('#cotizacion_email').val('');
                $('#cotizacion_atencion').val('');
                data_item = [];
                draw_list_item(data_item);
            }
        }
    });
}

function guardar_categoria(data){
    $.ajax({
        type: "POST",
        url: url+'categoria',
        data: {data},
        dataType: "json",
        beforeSend : function(){
        },
        success: function (response) {
            if(response.status=='ok'){
                toast_success(response.body);
                $('#inventario_categoria').val('');
            }
        }
    });
}

function guardar_unidadMedida(data){
    $.ajax({
        type: "POST",
        url: url+'unidadMedida',
        data: {data},
        dataType: "json",
        beforeSend : function(){
        },
        success: function (response) {
            if(response.status=='ok'){
                toast_success(response.body);
                $('#inventario_unidad').val('');
            }
        }
    });
}



function guardar_producto(data){
    $.ajax({
        type: "POST",
        url: url+'producto',
        data: {data},
        dataType: "json",
        beforeSend : function(){
        },
        success: function (response) {
            if(response.status=='ok'){
                swal_success(response.body);
                $('#inventario_producto_nombre').val('');
                $('#inventario_producto_descripcion').val('');
                $('#inventario_producto_medida').val('');
                $('#inventario_producto_precio').val('');
                $('#inventario_producto_existencia').val('');
                $('#inventario_producto_categoria').val(0);
                $('#inventario_producto_unidad').val(0);
            }
        }
    });
}
